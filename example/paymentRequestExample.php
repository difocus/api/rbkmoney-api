<?php
require '../vendor/autoload.php';

use ShopExpress\RbkMoney\Payment;

$apiToken = '';
$shopID = '';
$baseUrl = null;

$d = new Payment($apiToken, $shopID, 'RUB', $baseUrl);

$d->setProduct('Килограмм помидоров');
$d->setDescription('Килограмм отличных помидоров');
$d->setCartItem('Помидор', 5, 150);
$d->setCartItem('Доставка', 1, 100);
$d->setAmount(850); // в копейках, в центах
$d->setRedirectUrl('/success.php');
$d->setMetadata(['shop_order_id' => 5]);

echo $d->getPaymentForm();
