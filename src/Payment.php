<?php
namespace ShopExpress\RbkMoney;

use DateTime;
use ShopExpress\RbkMoney\Injector\LoggerInjector;

final class Payment
{
    use LoggerInjector;

    const ERROR_ARGUMENT_VALUE = 'Parameter `%s` can\'t be empty';

    const CREATE_INVOICE_TEMPLATE_DUE_DATE = 'Y-m-d\TH:i:s\Z';
    const CREATE_INVOICE_DUE_DATE = '+1 days';

    private $product;
    private $description;
    private $metadata = [];
    private $cart = [];
    private $total;

    /**
     * Hold expiration of the payment
     *      - cancel - отменить платеж и вернуть деньги
     *      - capture - списать в пользу магазина
     *
     * @var string
     */
    private $holdExpiration = 'capture'; // cancel
    private $redirectURL;
    private $currency = 'RUB';
    private $baseUrl = 'https://api.rbk.money/v1/processing/invoices';
    private $apiKey;
    private $shopID;

    private $content;
    private $data;

    /**
     * Webhooks callbacks
     */
    private $paidCallback;
    private $cancelledCallback;

    /**
     * @param string $apiKey The api key
     * @param string $shopID The shop id
     * @param string $currency The currency
     * @param string $baseUrl The base url
     */
    public function __construct(
        $apiKey,
        $shopID,
        $currency = null,
        $baseUrl = null
    ) {
        $this->apiKey = $apiKey;
        $this->shopID = $shopID;
        if (!is_null($currency) && !empty($currency)) {
            $this->currency = $currency;
        }
        if (!is_null($baseUrl) && !empty($baseUrl)) {
            $this->baseUrl = $baseUrl;
        }
    }

    /**
     * Sets the product.
     *
     * @param string $product The product
     *
     * @throws \InvalidArgumentException
     *
     * @return self
     */
    public function setProduct($product)
    {
        if (empty($product)) {
            throw new \InvalidArgumentException(sprintf(static::ERROR_ARGUMENT_VALUE, 'product'));
        }

        $this->product = $product;

        return $this;
    }

    /**
     * Sets the description.
     *
     * @param string $description The description
     *
     * @throws \InvalidArgumentException
     *
     * @return self
     */
    public function setDescription($description)
    {
        if (empty($description)) {
            throw new \InvalidArgumentException(sprintf(static::ERROR_ARGUMENT_VALUE, 'description'));
        }

        $this->description = $description;

        return $this;
    }

    /**
     * Sets the amount.
     *
     * @param int $amount The amount in minor monetary units
     *
     * @throws \InvalidArgumentException
     *
     * @return self
     */
    public function setAmount(int $amount)
    {
        if (empty($amount)) {
            throw new \InvalidArgumentException(sprintf(static::ERROR_ARGUMENT_VALUE, 'amount'));
        }

        $this->amount = (int)$amount;

        return $this;
    }

    /**
     * Sets the redirect url.
     *
     * @param string $redirectUrl The redirect url
     *
     * @throws \InvalidArgumentException
     *
     * @return self
     */
    public function setRedirectUrl($redirectUrl)
    {
        if (empty($redirectUrl)) {
            throw new \InvalidArgumentException(sprintf(static::ERROR_ARGUMENT_VALUE, 'redirectUrl'));
        }

        $this->redirectUrl = $redirectUrl;

        return $this;
    }

    /**
     * Sets the metadata.
     *
     * @param array $metadata The metadata
     *
     * @return self
     */
    public function setMetadata(array $metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Sets the hold expiration.
     *
     * @param string $holdExpiration The hold expiration
     *
     * @throws \InvalidArgumentException
     *
     * @return self
     */
    public function setHoldExpiration($holdExpiration)
    {
        $validValues = ['capture', 'cancel'];
        if (!in_array($holdExpiration, $validValues)) {
            throw new \InvalidArgumentException(
                'Parameter `holdExpiration` must match one of the values: ' . implode(', ', $validValues)
            );
        }

        $this->holdExpiration = $holdExpiration;

        return $this;
    }

    /**
     * Sets the cart item.
     *
     * @param string $name The name
     * @param int $qty The quantity
     * @param int $price The price in minor monetary units
     * @param string $tax The tax for ex.: "0%", "10%", "18%", "10/110", "18/118"
     */
    public function setCartItem($name, int $qty, int $price, $tax = '0%')
    {
        $validValues = ["0%", "10%", "18%", "10/110", "18/118"];
        if (!in_array($tax, $validValues)) {
            throw new \InvalidArgumentException(
                'Parameter `tax` must match one of the values: ' . implode(', ', $validValues)
            );
        }

        $this->cart[] = [
            'product' => $name,
            'quantity' => (int)$qty,
            'price' => (int)$price,
            'taxMode' => [
                'type' => 'InvoiceLineTaxVAT',
                'rate' => $tax,
            ],
        ];
    }

    /**
     * Gets the payment form.
     *
     * @throws \Exception
     *
     * @return string The payment form HTML code.
     */
    public function getPaymentForm()
    {
        if (!count($this->cart)) {
            throw new \Exception('Error cart can\'t be empty');
        }

        if (empty($this->redirectUrl)) {
            throw new \Exception('Error `redirectUrl` can\'t be empty');
        }

        $response = $this->createInvoiceRequest();

        if (!isset($response['invoice'])) {
            if (isset($response['description'])) {
                $message = $response['description'];
            } else {
                $message = $response['message'];
            }
            $this->getLogger()->error($message, [$response]);
            
            throw new \Exception($message);
        }

        $string = "<form action='{$this->redirectUrl}' method='POST'>";
        $string .=  '<script src="https://checkout.rbk.money/checkout.js" class="rbkmoney-checkout"';
        $string .=      " data-invoice-id='{$response['invoice']['id']}'";
        $string .=      " data-invoice-access-token='{$response['invoiceAccessToken']['payload']}'";
        $string .=      " data-payment-flow-hold='true'";
        $string .=      " data-hold-expiration='{$this->holdExpiration}'";
        $string .=      " data-name='{$this->product}'";
        $string .=      " data-label='Оплатить'";
        $string .=      " data-description='{$this->description}'";
        $string .=      " data-pay-button-label='Оплатить'>";
        $string .=    '</script>';
        $string .= '</form>';

        return $string;
    }

    /**
     * Parse payment reponse input from RBKmoney server.
     *
     * @return array The data array from RBKmoney
     */
    public function parseInput()
    {
        $this->content = file_get_contents('php://input');
        $this->data = json_decode($this->content, true);

        $requiredFields = ['invoice', 'eventType'];
        foreach ($requiredFields as $field) {
            if (empty($this->data[$field])) {
                $this->getLogger()->error('One or more required fields are missing', [$this->data]);
                return false;
            }
        }
        
        $this->getLogger()->info('content', [$this->data]);

        return $this->data;
    }

    /**
     * Validate payment response from RBKmoney server.
     *
     * @param string $publicKey The public key
     *
     * @return void
     */
    public function validate($publicKey)
    {
        if (!isset($_SERVER['HTTP_CONTENT_SIGNATURE'])) {
            $this->jsonResponse(400, 'Error `HTTP_CONTENT_SIGNATURE` can\'t be empty!');
            return false;
        }

        $signatureFromHeader = $this->getSignatureFromHeader($_SERVER['HTTP_CONTENT_SIGNATURE']);
        $decodedSignature = $this->urlsafeB64decode($signatureFromHeader);

        if (!$this->verificationSignature($this->content, $decodedSignature, $publicKey)) {
            $this->jsonResponse(400, 'Webhook notification signature mismatch');
            return false;
        }

        if ($this->data['invoice']['shopID'] != $this->shopID) {
            $this->jsonResponse(400, "Received `shopID` {$this->shopID} mismatch {$this->data['invoice']['shopID']}");
            return false;
        }

        if (empty($this->amount)) {
            $this->jsonResponse(400, 'Error `amount` can\'t be empty!');
            return false;
        }

        $invoice_amount = (int)$this->data['invoice']['amount'];
        if ($this->amount != $invoice_amount) {
            $this->jsonResponse(400, "Received amount {$this->amount} mismatch {$this->data['invoice']['amount']}");
            return false;
        }
        
        $this->jsonResponse(200, 'OK');

        $allowedEventTypes = ['InvoicePaid', 'InvoiceCancelled'];
        if (in_array($this->data['eventType'], $allowedEventTypes)) {
            $invoiceStatus = $this->data['invoice']['status'];
            if ($invoiceStatus == 'paid') {
                return true;
            } elseif ($invoiceStatus == 'cancelled') {
                return false;
            }
        }

        return true;
    }

    /**
     * Creates an invoice request.
     *
     * @throws \Exception
     *
     * @return array The response from the RBKmoney server.
     */
    private function createInvoiceRequest()
    {
        $data = [
            'shopID' => $this->shopID,
            'dueDate' => $this->getDueDate(),
            'amount' => $this->amount,
            'currency' => $this->currency,
            'product' => $this->product,
            'description' => $this->description,
            'cart' => $this->cart,
        ];

        if (count($this->metadata)) {
            $data['metadata'] = $this->metadata;
        }

        $curl = curl_init();

        curl_setopt_array(
            $curl,
            [
                CURLOPT_URL => $this->baseUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_HTTPHEADER => $this->prepareHeaders($this->apiKey),
            ]
        );

        $resultBody = curl_exec($curl);
        $errno = curl_errno($curl);
        $error = curl_error($curl);

        curl_close($curl);

        if ($resultBody === false) {
            throw new \Exception(
                sprintf('Curl error: %s (%s)', $errno, $error)
            );
        }

        if (empty($resultBody)) {
            throw new \Exception('Empty response!');
        }

        return json_decode($resultBody, true);
    }

    /**
     * @param string $apiKey The api key
     *
     * @return array
     */
    private function prepareHeaders($apiKey)
    {
        $headers = [];
        $headers[] = 'X-Request-ID: ' . uniqid();
        $headers[] = 'Authorization: Bearer ' . $apiKey;
        $headers[] = 'Content-type: application/json; charset=utf-8';
        $headers[] = 'Accept: application/json';
        return $headers;
    }

    /**
     * Generates the due date.
     *
     * @return string The due date.
     */
    private function getDueDate()
    {
        $currentTimezone = date_default_timezone_get();
        date_default_timezone_set('UTC');
        $date = date(static::CREATE_INVOICE_TEMPLATE_DUE_DATE, strtotime(static::CREATE_INVOICE_DUE_DATE));
        date_default_timezone_set($currentTimezone);
        return $date;
    }

    /**
     * @param string $string The string
     *
     * @return string
     */
    private function urlsafeB64decode($string)
    {
        $data = str_replace(array('-', '_'), array('+', '/'), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    /**
     * Gets the signature from header.
     *
     * @param string $contentSignature The content signature
     *
     * @throws \Exception
     *
     * @return string The signature from header.
     */
    private function getSignatureFromHeader($contentSignature)
    {
        $signature = preg_replace("/alg=(\S+);\sdigest=/", '', $contentSignature);

        if (empty($signature)) {
            throw new \Exception('Signature is missing');
        }

        return $signature;
    }

    /**
     * Verify the signature.
     *
     * @param string $data The data
     * @param string $signature The signature
     * @param string $publicKey The public key
     *
     * @return boolean
     */
    private function verificationSignature($data, $signature, $publicKey)
    {
        if (empty($data) || empty($signature) || empty($publicKey)) {
            return false;
        }

        $publicKeyId = openssl_get_publickey($publicKey);
        if (empty($publicKeyId)) {
            return false;
        }

        $verify = openssl_verify($data, $signature, $publicKeyId, OPENSSL_ALGO_SHA256);

        return ($verify == 1);
    }

    /**
     * @param string $code The code
     * @param string $message The message
     */
    public function jsonResponse($code, $message)
    {
        $this->getLogger()->error($message);

        http_response_code($code);
        echo json_encode(['message' => $message]);
    }
}
